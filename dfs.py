# lets refacotor
from stack import Stack

def dfs(self, starting_vertex_id, target_value):
    s = Stack()
    s.push([starting_vertex_id])
    visited = set()
    while s.size() > 0:
        path = s.pop()
        v = path[-1]
        if v not in visited:
            if v == target_value:
                return path
            visited.add(v)
            for next_vert in self.get_neighbors(v):
                new_path = list(path)
                new_path.append(next_vert)
                s.push(new_path)
    return None

def dfs_recursive(self, starting_vertex, target_value, visited=None, path=None):
    """
    Return a list containing a path from
    starting_vertex to target_value in
    depth-first order. given that the target_value exists in the graph
    This should be done using recursion.
    """

    if visited is None:
        visited = set()
    
    if path is None:
        path = []

    # add starting vertex to the visited set
    visited.add(starting_vertex)

    # concatonate the starting vert path to the current path
    path = path + [starting_vertex]

    # check if the starting vertex is equal to the target value
    if starting_vertex == target_value:
        return path

    for child_vertex in self.get_neighbors(starting_vertex):
        if child_vertex not in visited:
            new_path = self.dfs_recursive(child_vertex, target_value, visited, path)

            if new_path:
                return new_path
    
    return None