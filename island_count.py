  
"""
Remember these steps to solve almost any graphs problem:
- Translate the problem into terminology you've learned this week
- Build your graph
- Traverse your graph

ISLANDS MATRIX CHALLENGE!
--------------------------
Write a function that takes a 2D binary array and returns the number of 1 islands. An island consists of 1s that are connected to the north, south, east or west. For example:
islands = [[0, 1, 0, 1, 0],
           [1, 1, 0, 1, 1],
           [0, 0, 1, 0, 0],
           [1, 0, 1, 0, 0],
           [1, 1, 0, 0, 0]]
island_counter(islands) # returns 4

step 1
Node == 1 in the data set
Edge == connection to the north, south, east, or west
Algo = DFT

"""

from stack import Stack


def island_counter(matrix):
    # counter
    island_count = 0
    # step 2: create a visited matrix (build your graph)

    # step 3: walk through each cel in the matrix (traverse your graph)
        # if it has not been visited...
            # when we reach a 1...
                # do a DFT and mark each as visited
                # then increment a counter

    return island_count

def dft(row, col, matrix, visted):
    """
    This will mark each connected component as visited
    return a visited matrix
    """
    pass



def get_neighbors(row, col, graph_matrix):
    neighbors = []

    # do some stuff (logic for the specific case)

    # neighbors = north, south, east, west

    # if north (row - 1, col)
        # do the logic (append if a 1)
    # if south (row + 1, col)
        # do the logic (append if a 1)
    # if east (row, col + 1)
        # do the logic (append if a 1)
    # if west (row, col - 1)
        # do the logic (append if a 1)

    return neighbors


# tests

islands = [[0, 1, 0, 1, 0],
           [1, 1, 0, 1, 1],
           [0, 0, 1, 0, 0],
           [1, 0, 1, 0, 0],
           [1, 1, 0, 0, 0]]

i1 = island_counter(islands)  # 4

islands = [[1, 0, 0, 1, 1, 0, 1, 1, 0, 1],
           [0, 0, 1, 1, 0, 1, 0, 0, 0, 0],
           [0, 1, 1, 1, 0, 0, 0, 1, 0, 1],
           [0, 0, 1, 0, 0, 1, 0, 0, 1, 1],
           [0, 0, 1, 1, 0, 1, 0, 1, 1, 0],
           [0, 1, 0, 1, 1, 1, 0, 1, 0, 0],
           [0, 0, 1, 0, 0, 1, 1, 0, 0, 0],
           [1, 0, 1, 1, 0, 0, 0, 1, 1, 0],
           [0, 1, 1, 0, 0, 0, 1, 1, 0, 0],
           [0, 0, 1, 1, 0, 1, 0, 0, 1, 0]]

i2 = island_counter(islands)  # 13

print(i1) # 4
print(i2) # 13