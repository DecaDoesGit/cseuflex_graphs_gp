from stack import Stack

# Stack data structure and a set data structure (hash table)
def dft(self, starting_vertex_id):
    # Create an empty stack and push the starting vertex ID
    s = Stack()
    s.push(starting_vertex_id)
    # Create a Set to store visited vertices
    visited = set()
    # While the stack is not empty...
    while s.size() > 0:
        # Pop the first vertex
        v = s.pop()
        # If that vertex has not been visited...
        if v not in visited:
            # Mark it as visited...
            print(v)  # for debug
            visited.add(v)

            # Then add all of its neighbors to the back of the stack
            for next_v in self.get_neighbors(v):
                s.push(next_v)