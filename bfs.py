from queue import Queue

# def bfs(self, starting_vertex_id, target_vertex_id):
#     # Create an empty queue and enqueue A PATH TO the starting vertex ID
#     q = Queue()
#     q.enqueue([starting_vertex_id])
#     # Create a Set to store visited vertices
#     # same as bft
#     # While the queue is not empty...
#     # same as bft
#         # Dequeue the first PATH
#         # same as bft
#         # Grab the last vertex from the PATH
#         # how do we get the last item in a list?
#         # If that vertex has not been visited...
#         # same as bft
#             # CHECK IF IT'S THE TARGET
#             # this is our search part
#               # IF SO, RETURN PATH
#             # Mark it as visited...
#             # same as bft
#             # Then add A PATH TO its neighbors to the back of the queue
#               # new
#               # COPY THE PATH
#               # APPEND THE NEIGHOR TO THE BACK


def bfs(self, starting_vertex_id, target_value):
    q = Queue()
    q.enqueue([starting_vertex_id])
    visited = set()
    while q.size() > 0:
        path = q.dequeue()
        v = path[-1]
        if v not in visited:
            if v == target_value:
                return path
            visited.add(v)
            for next_vert in self.get_neighbors(v):
                new_path = list(path)
                new_path.append(next_vert)
                q.enqueue(new_path)
    return None