from queue import Queue

# Queue data structure and a set data structure (hash table)
def bft(self, starting_vertex_id):
    # Create an empty queue and enqueue the starting vertex ID
    q = Queue()
    q.enqueue(starting_vertex_id)
    # Create a Set to store visited vertices
    visited = set()
    # While the queue is not empty...
    while q.size() > 0:
        # Dequeue the first vertex
        v = q.dequeue()
        # If that vertex has not been visited...
        if v not in visited:
            # Mark it as visited...
            print(v)  # for debug
            visited.add(v)

            # Then add all of its neighbors to the back of the queue
            for next_v in self.get_neighbors(v):
                q.enqueue(next_v)