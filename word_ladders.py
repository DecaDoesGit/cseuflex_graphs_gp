# lets think of what to do

"""
    Node == Word
    Edge == One Letter Change
    Algo == BFS with path (shortest, transformation sequence)
"""

from queue import Queue

# open the words.txt

f = open('words.txt', 'r')
words = f.read().split('\n')
# word set
word_set = set()

for word in words:
    word_set.add(word.lower())

# letters
letters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']


# BFS with Path

def find_ladders(begin_word, end_word):
    q = Queue()
    q.enqueue([begin_word])
    visited = set()
    while q.size() > 0:
        path = q.dequeue()
        v = path[-1]
        if v not in visited:
            if v == end_word:
                return path
            visited.add(v)
            for next_vert in get_neighbors(v):
                new_path = list(path)
                new_path.append(next_vert)
                q.enqueue(new_path)
    return None


def get_neighbors(word):
    neighbors = []
    string_word = list(word)

    for i in range(len(string_word)):
        for letter in letters:
            temp_word = list(string_word) # make a copy of the list
            temp_word[i] = letter
            w = "".join(temp_word)

            if w != word and w in word_set:
                neighbors.append(w)

    return neighbors


print(find_ladders("sail", "boat")) # ['sail', 'bail', 'boil', 'boll', 'bolt', 'boat']