import random
from queue import Queue
from stack import Stack
import time

class User:
    def __init__(self, name):
        self.name = name

class SocialGraph:
    def __init__(self):
        self.last_id = 0
        self.users = {}
        self.friendships = {}

    def add_friendship(self, user_id, friend_id):
        """
        Creates a bi-directional friendship
        """
        if user_id == friend_id:
            return False
            # print("WARNING: You cannot be friends with yourself")
        elif friend_id in self.friendships[user_id] or user_id in self.friendships[friend_id]:
            # print("WARNING: Friendship already exists")
            return False
        else:
            self.friendships[user_id].add(friend_id)
            self.friendships[friend_id].add(user_id)
            return True

    def add_user(self, name):
        """
        Create a new user with a sequential integer ID
        """
        self.last_id += 1  # automatically increment the ID to assign the new user
        self.users[self.last_id] = User(name)
        self.friendships[self.last_id] = set()

    def populate_graph(self, num_users, avg_friendships): # O(n^2)
        """
        Takes a number of users and an average number of friendships
        as arguments
        Creates that number of users and a randomly distributed friendships
        between those users.
        The number of users must be greater than the average number of friendships.
        """
        # Reset graph
        self.last_id = 0
        self.users = {}
        self.friendships = {}
        # !!!! IMPLEMENT ME

        # Add users - node / vertex
        # iterate over users and add them
        for i in range(0, num_users):
            self.add_user(f"User {i}")

        # Create friendships - connection / edge
        # Generate all possible friendship combinations
        possible_friendships = []
        # Avoid duplicates by ensuring the first number is smaller than the second
        # if n = 10 add_friendships will be run 100 times
        for user_id in self.users:
            for friend_id in range(user_id + 1, self.last_id + 1):
                possible_friendships.append((user_id, friend_id))
        # Shuffle the possible friendships
        random.shuffle(possible_friendships)
        # Create friendships for the first X pairs of the list
        # X is determined by the formula: num_users * avg_friendships // 2
        # Need to divide by 2 since each add_friendship() creates 2 friendships
        for i in range(num_users * avg_friendships // 2):
            friendship = possible_friendships[i]
            self.add_friendship(friendship[0], friendship[1])

    def populate_graph_linear(self, num_users, avg_friendships): # O(n)
        """
        Takes a number of users and an average number of friendships
        as arguments
        Creates that number of users and a randomly distributed friendships
        between those users.
        The number of users must be greater than the average number of friendships.
        """
        # Reset graph
        self.last_id = 0
        self.users = {}
        self.friendships = {}
        # !!!! IMPLEMENT ME

        # Add users - node / vertex
        # iterate over users and add them
        for i in range(0, num_users):
            self.add_user(f"User {i + 1}")
        
        target_friendships = (num_users * avg_friendships)
        total_friendships = 0
        collisions = 0
        # if n = 10 then add friendships would be run 10 times
        while total_friendships < target_friendships:
            user_id = random.randint(1, self.last_id) # O(1)
            friend_id = random.randint(1, self.last_id) # O(1)
            if self.add_friendship(user_id, friend_id):
                total_friendships += 1
            else:
                collisions += 1
        
        print(f"COLLISIONS: {collisions}")


    def get_all_social_paths(self, user_id): # quadratic
        """
        Takes a user's user_id as an argument
        Returns a dictionary containing every user in that user's
        extended network with the shortest friendship path between them.
        The key is the friend's ID and the value is the path.
        """
        visited = {}  # Note that this is a dictionary, not a set
        # !!!! IMPLEMENT ME
        q = Queue()
        q.enqueue([user_id])

        while q.size() > 0: # while the queue is not empty
            path = q.dequeue()

            v = path[-1]

            if v not in visited:
                visited[v] = path
                for neighbor in self.friendships[v]:
                    new_path = path.copy()
                    new_path.append(neighbor)
                    q.enqueue(new_path)
        return visited


if __name__ == '__main__':

    num_users = 2000
    avg_friendships = 500
    sg = SocialGraph()
    start_time = time.time()
    sg.populate_graph(num_users, avg_friendships)
    end_time = time.time()
    print(f"Quadratic Runtime: {end_time - start_time} seconds")
    start_time = time.time()
    sg.populate_graph_linear(num_users, avg_friendships)
    end_time = time.time()
    print(f"Linear Runtime: {end_time - start_time} seconds")